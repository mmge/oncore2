recodeAllFields <- function(queryObject) {
  query <- queryObject$query
  lookup_table <- lookup_table()
  lookup_table <- lookup_table[lookup_table$CATEGORY != "UOM", ]
  column_names <- colnames(queryObject$query)
  for(cn in column_names) {
    query_codes <- suppressWarnings(as.numeric(unique(query[, cn])))
    query_codes <- query_codes[!is.na(query_codes)]
    if(length(query_codes) > 0) {
      lookup_categories <- unique(lookup_table$CATEGORY[lookup_table$CODE_ID %in% query_codes])
      if(length(lookup_categories) > 1 && "EDC" %in% lookup_categories) {
        cat_check <- lookup_categories[lookup_categories != "EDC"]
      } else {
        cat_check <- lookup_categories

      }
      if(length(cat_check) == 1) {
        table_codes <- lookup_table[lookup_table$CATEGORY %in% lookup_categories, "CODE_ID"]
        if(sum(query_codes %in% table_codes) == length(query_codes)) {
          query <- recode_field(query, cn, category = lookup_categories)
        }
      } else if(length(lookup_categories) > 1) {
        if(length(lookup_categories > 5)) {
          message(length(lookup_categories), " category matches for: ", cn)
        } else {
          message("Multiple category matches for: ", cn, "\n", paste("\t", lookup_categories, collapse = "\n"))
        }
      }
    }
  }
  queryObject$query <- query
  return(queryObject)
}

